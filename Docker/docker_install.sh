#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${DIR}

# Obtain graphdb as wget is not working for some reason in docker 20.04
# wget "http://download.ontotext.com/owlim/a2aac65e-d5a3-11ea-a9ef-42843b1b6b38/graphdb-free-9.3.3-dist.zip?utm_campaign=GraphDB&utm_medium=email&_hsmi=92670193&_hsenc=p2ANqtz-_6wkDuK_P0VgpDSok777zvpgRNpNQiCM6FBlL02vS_quif1tMbxJL7yUIn-IWP3XupE5PrZNfSR1uX_xvP2ykvQqLCTQ&utm_content=92670193&utm_source=hs_email" -O graphdb.zip
# unzip graphdb.zip
# rm graphdb.zip

# To build the docker image
docker build -t munlock/notebook .

# To run the test, obtain exit code and if all went well upload the file
docker run --rm -p 8888:8888 -p 7200:7200 munlock/notebook

ret=$?

if [[ ${ret} -eq 0 ]]; then
    # To upload the docker image
    echo "uploading"
    docker push munlock/notebook
fi

if [[ ${ret} -ne 0 ]]; then
    # To upload the docker image
    echo "Test failed"
fi

# Cleaning up the generic file
# rm -rf Generic