echo Start this docker image as followed:
echo docker run -p 8888:8888 -p 7200:7200 -v \`pwd\`:/home/jovyan/export munlock/notebook
echo ========================================
echo = Starting graphdb port 7200
echo = Starting jupyter notebook port 8888
echo = Folder should be mounted to $HOME/export
echo ========================================
# If export folder exists....
if [ ! -d ./export/graphdb-free ]; then
	echo export folder does not exist, creating temporarily one
	mkdir export
fi

if [ ! -d ./export/graphdb-free ]; then
    mv ./graphdb-free ./export/graphdb-free
fi

./export/graphdb-free/bin/graphdb &
jupyter-lab --port=8888 --no-browser --ip=0.0.0.0 --notebook-dir=$HOME --allow-root
